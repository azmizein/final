import Footer from "../../component/footer";
import MessageDoctor from "../../component/messageDoctor";
import Navbar from "../../component/navbar";

export default function PageDoctor() {
  return (
    <>
      <Navbar />
      <MessageDoctor />
      <Footer />
    </>
  );
}
