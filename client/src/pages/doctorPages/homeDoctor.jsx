import Footer from "../../component/footer";
import MessageDoctor from "../../component/messageDoctor";
import Nav from "../../component/navbarDoctor";

export default function HomeDoctor() {
  return (
    <>
      <Nav />
      <MessageDoctor />
      <Footer />
    </>
  );
}
