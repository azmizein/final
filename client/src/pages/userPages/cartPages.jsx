import CartDetail from "../../component/cart";
import Footer from "../../component/footer";
import Navbar from "../../component/navbar";

export default function CartPage() {
  return (
    <>
      <Navbar />
      <CartDetail />
      <Footer />
    </>
  );
}
