import Footer from "../../component/footer";
import Navbar from "../../component/navbar";
import Transaction from "../../component/transaksi";

export default function TransactionPage() {
  return (
    <>
      <Navbar />
      <Transaction />
      <Footer />
    </>
  );
}
