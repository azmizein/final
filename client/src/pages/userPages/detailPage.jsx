import DetailComp from "../../component/detail";
import Footer from "../../component/footer";
import Navbar from "../../component/navbar";

export default function DetailPage() {
  return (
    <>
      <Navbar />
      <DetailComp />
      <Footer />
    </>
  );
}
