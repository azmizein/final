import Footer from "../../component/footer";
import Blog from "../../component/homeBlog";
import Navbar from "../../component/navbar";

export default function HomeBlogPage() {
  return (
    <>
      <Navbar />
      <Blog />
      <Footer />
    </>
  );
}
