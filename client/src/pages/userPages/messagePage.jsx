import Footer from "../../component/footer";
import Message from "../../component/message";
import Navbar from "../../component/navbar";

export default function MessagePage() {
  return (
    <>
      <Navbar />
      <Message />
      <Footer />
    </>
  );
}
