import Footer from "../../component/footer";
import Navbar from "../../component/navbar";
import Profile from "../../component/profile";

export default function ProfilPage() {
  return (
    <>
      <Navbar />
      <Profile />
      <Footer />
    </>
  );
}
