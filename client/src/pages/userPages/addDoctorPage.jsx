import AddDoctor from "../../component/addDoctor";
import Footer from "../../component/footer";
import Navbar from "../../component/navbar";

export default function AddDoctorPage() {
  return (
    <>
      <Navbar />
      <AddDoctor />
      <Footer />
    </>
  );
}
