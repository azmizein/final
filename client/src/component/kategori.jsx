import {
  Box,
  Text,
  Image,
  Center,
  SimpleGrid,
  useColorModeValue,
  useBreakpointValue,
} from "@chakra-ui/react";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";
import axios from "axios";
import { syncCategory } from "../redux/categorySlice";
import { syncData } from "../redux/bookSlice";

export default function Category() {
  const dispatch = useDispatch();
  const data = useSelector((state) => state.categorySlice.value);

  const getData = async () => {
    try {
      const res = await axios.get("http://localhost:2000/product/category");
      const data = res.data;
      dispatch(syncCategory(data));
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleCategoryClick = async (CategoryId) => {
    try {
      const res = await axios.get(
        `http://localhost:2000/product/listCat/${CategoryId}`
      );
      const data = res.data;
      dispatch(syncData(data));
    } catch (err) {
      console.log(err);
    }
  };

  const lightGreen = useColorModeValue("teal.200", "teal.700");

  const columns = useBreakpointValue({ base: 2, md: 5 });

  return (
    <>
      <Center>
        <Text
          fontWeight="bold"
          mt="100px"
          color="#555"
          fontSize={{ base: "16px", md: "20px" }}>
          Belanja Sesuai Kategori
        </Text>
      </Center>
      <Center mt="20px">
        <SimpleGrid columns={columns} spacing={4}>
          {data?.map((item) => (
            <Box
              key={item.id}
              w={columns === 2 ? "100%" : "200px"}
              h="80px"
              borderRadius="10px"
              bg="#fafafa"
              display="flex"
              flexDirection="row"
              _hover={{ bg: lightGreen, color: "white" }}
              p="2"
              onClick={() => handleCategoryClick(item.id)}
              cursor="pointer">
              <Box margin="auto">
                <Image
                  src={item.images}
                  w="60px"
                  h="60px"
                  cursor="pointer"
                  bg="#ebf5e9"
                  rounded="full"
                  objectFit="cover"
                />
              </Box>
              <Box margin="auto" w="100px">
                <Text
                  fontSize="small"
                  color="#285430"
                  align="center"
                  fontWeight="bold">
                  {item.categoryName}
                </Text>
              </Box>
            </Box>
          ))}
        </SimpleGrid>
      </Center>
    </>
  );
}
