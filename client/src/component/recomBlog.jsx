import { useEffect } from "react";
import {
  Box,
  Flex,
  Image,
  Text,
  Button,
  useColorModeValue,
  Divider,
} from "@chakra-ui/react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { syncDataPost } from "../redux/postSlice";
import { Link } from "react-router-dom";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function RecommendedBlog() {
  const url = `http://localhost:2000/post/getPost`;
  const dispatch = useDispatch();
  const data = useSelector((state) => state.postSlice.value);

  const getData = async () => {
    try {
      const res = await axios.get(url);
      const data = res.data;
      dispatch(syncDataPost(data));
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const lightGreen = useColorModeValue("teal.200", "teal.700");

  const settings = {
    dots: true,
    infinite: true,
    speed: 700,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    centerMode: true,
  };

  const responsiveSettings = [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
      },
    },
  ];

  return (
    <>
      <Divider color="red" mt="50px" />
      <Box mt="5" py={{ base: "20px", md: "50px" }}>
        <Box
          fontSize={{ base: "16px", md: "20px" }}
          color="#555"
          fontWeight="bold"
          textAlign="center">
          Baca Artikel Terbaru
        </Box>
        <Slider {...settings} responsive={responsiveSettings}>
          {data.map((item) => (
            <Flex
              mt="10"
              className="post"
              key={item.id}
              flexDirection="column"
              width={{ base: "100%", md: "45%", lg: "30%" }}
              height="350px"
              maxWidth="400px"
              alignItems="center"
              justifyContent="space-between"
              border="1px solid #ddd"
              borderRadius="20px"
              boxShadow="md"
              mx={{ base: "auto", md: "10px" }}>
              <Image
                src={`http://localhost:2000/Public/${item.image}`}
                alt=""
                w="100%"
                h="200px"
                borderTopRadius="20px"
                objectFit="cover"
              />
              <Text fontSize="18px" color="#555" h="90px" ml="20px" mr="20px">
                {item.title}
              </Text>
              <Button
                as={Link}
                to={`/readBlog/${item.id}`}
                ml="20px"
                width="max-content"
                border="1px solid teal"
                cursor="pointer"
                color="teal"
                backgroundColor="white"
                borderWidth="1px"
                borderColor="teal"
                _hover={{
                  borderColor: "white",
                  backgroundColor: lightGreen,
                  color: "black",
                }}>
                Read More
              </Button>
            </Flex>
          ))}
        </Slider>
      </Box>
      <Box textAlign="center" mt="20px" mb="20px">
        <Link to="/blog" style={{ textDecoration: "none", color: "teal" }}>
          Baca Selengkapnya
        </Link>
      </Box>
    </>
  );
}
