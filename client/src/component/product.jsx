/* eslint-disable react/no-children-prop */
import { useEffect, useState } from "react";
import {
  Box,
  Button,
  Icon,
  Text,
  Image,
  Flex,
  Center,
  useColorModeValue,
  Input,
  InputGroup,
  InputLeftElement,
  FormControl,
  FormLabel,
} from "@chakra-ui/react";
import axios from "axios";
import { IoCartOutline } from "react-icons/io5";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { useDispatch, useSelector } from "react-redux";
import { syncData } from "../redux/bookSlice";
import { cartSync } from "../redux/cartSlice";
import { addCart } from "../redux/userSlice";
import { BsSortAlphaDown, BsSortAlphaUp } from "react-icons/bs";
import { SearchIcon } from "@chakra-ui/icons";
import { BsFilterLeft } from "react-icons/bs";
import { BiReset } from "react-icons/bi";

export default function ProductCard() {
  const url = `http://localhost:2000/product/list`;
  const data = useSelector((state) => state.bookSlice.value);
  const dispatch = useDispatch();
  const { id } = useSelector((state) => state.userSlice.value);

  const getData = async () => {
    try {
      const res = await axios.get(url);
      const data = res.data;
      dispatch(syncData(data));
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const [sortOrder, setSortOrder] = useState("asc");
  const [sortedData, setSortedData] = useState([]);

  const sortData = (data, order) => {
    const sorted = [...data];
    sorted.sort((a, b) => {
      if (order === "asc") {
        return a.name.localeCompare(b.name);
      } else {
        return b.name.localeCompare(a.name);
      }
    });
    return sorted;
  };

  useEffect(() => {
    setSortedData(sortData(data, sortOrder));
  }, [data, sortOrder]);

  const [searchQuery, setSearchQuery] = useState("");
  const [filteredData, setFilteredData] = useState([]);

  const handleSearchInputChange = (event) => {
    setSearchQuery(event.target.value);
  };

  useEffect(() => {
    const filtered = sortedData.filter((item) =>
      item.name.toLowerCase().includes(searchQuery.toLowerCase())
    );
    setFilteredData(filtered);
  }, [searchQuery, sortedData]);

  const formatIDRCurrency = (amount) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(amount);
  };

  const itemsPerPage = 16;
  const [currentPage, setCurrentPage] = useState(1);
  const indexOfLastItem = currentPage * itemsPerPage;
  const indexOfFirstItem = indexOfLastItem - itemsPerPage;
  const currentData = filteredData.slice(indexOfFirstItem, indexOfLastItem);

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const onAddCart = async (ProductId) => {
    try {
      if (!id) {
        return Swal.fire({
          icon: "error",
          title: "Oooops ...",
          text: "Anda harus login terlebih dahulu",
          timer: 2000,
          customClass: {
            container: "my-swal",
          },
        });
      }

      const result = await axios.post("http://localhost:2000/cart/add", {
        UserId: id,
        ProductId,
      });

      const res = await axios.get(`http://localhost:2000/cart/${id}`);
      dispatch(cartSync(res.data));
      dispatch(addCart());
      getData();

      Swal.fire({
        icon: "success",
        title: "Berhasil",
        text: `${result.data.message}`,
        timer: 2000,
        customClass: {
          container: "my-swal",
        },
      });
    } catch (err) {
      console.log(err);
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: `${err.response.data}`,
        customClass: {
          container: "my-swal",
        },
      });
    }
  };

  const lightGreen = useColorModeValue("teal.200", "teal.700");

  return (
    <>
      <Center mt="30px" id="products">
        <Flex flexWrap={"wrap"} color={useColorModeValue("black", "white")}>
          <Box className="filter">
            <Box
              m="10px"
              mb="20px"
              borderWidth="1px"
              boxShadow="md"
              borderRadius="7px">
              <Box
                alignItems={"center"}
                h="50px"
                borderTopRadius="7px"
                align="center"
                bg={lightGreen}
                display="flex">
                <Box h="25px" ml="10px">
                  <Icon boxSize="6" as={BsFilterLeft} />
                </Box>
                <Box h="25px">
                  <Text mx="10px" fontWeight="bold">
                    Pengurutan & Pencarian
                  </Text>
                </Box>
                <Icon
                  sx={{ _hover: { cursor: "pointer" } }}
                  boxSize="6"
                  as={BiReset}
                />
              </Box>
              <Flex m={2} wrap="wrap">
                <FormControl w="" m={1}>
                  <FormLabel fontSize="x-small">Pilih Produk</FormLabel>
                  <Button
                    onClick={() => setSortOrder("asc")}
                    _hover={{ cursor: "pointer", bg: lightGreen }}
                    variant={sortOrder === "asc" ? "solid" : "outline"}
                    colorScheme="teal"
                    mx="2px"
                    size="md"
                    fontSize={{
                      base: "xs",
                      sm: "sm",
                      md: "md",
                      lg: "lg",
                      xl: "xl",
                    }}>
                    <BsSortAlphaDown /> Pengurutan A-Z
                  </Button>
                  <Button
                    onClick={() => setSortOrder("desc")}
                    _hover={{ cursor: "pointer", bg: lightGreen }}
                    variant={sortOrder === "desc" ? "solid" : "outline"}
                    colorScheme="teal"
                    mx="2px"
                    size="md"
                    fontSize={{
                      base: "xs",
                      sm: "sm",
                      md: "md",
                      lg: "lg",
                      xl: "xl",
                    }}>
                    <BsSortAlphaUp /> Pengurutan Z-A
                  </Button>
                  <FormLabel fontSize="x-small" mt="20px">
                    Cari Produk
                  </FormLabel>
                  <InputGroup mb="5px">
                    <InputLeftElement
                      pointerEvents="none"
                      children={<SearchIcon color="gray.300" />}
                    />
                    <Input
                      color={lightGreen}
                      type="text"
                      placeholder="Cari Produk..."
                      value={searchQuery}
                      onChange={handleSearchInputChange}
                      size="md"
                      borderRadius="5px"
                      borderWidth="2px"
                      borderColor={lightGreen}
                      paddingLeft="12"
                      _placeholder={{ color: "white" }}
                    />
                  </InputGroup>
                </FormControl>
              </Flex>
            </Box>
          </Box>
        </Flex>
      </Center>

      <Center>
        <Flex flexWrap={"wrap"} justifyContent="center" mt="30px">
          {currentData.map((item) => {
            return (
              <Box
                key={item.id}
                w="180px"
                h="293px"
                borderWidth="1px"
                m="10px"
                _hover={{ boxShadow: "xl" }}
                boxShadow="base"
                borderRadius="13px">
                <Box
                  h="155px"
                  w="full"
                  _hover={{ cursor: "pointer" }}
                  borderTopRadius="13px"
                  overflow="hidden">
                  <Image
                    objectFit="cover"
                    src={`http://localhost:2000/public/${item.images}`}
                    width="180px"
                    height="155px"
                  />
                </Box>
                <Box px="10px" h="90px">
                  <Box h="50px" as={Link} to={`/detail/${item.id}`}>
                    <Text
                      _hover={{ cursor: "pointer", color: lightGreen }}
                      fontWeight="bold">
                      {item.name.substring(0, 25)}
                      {item.name.length >= 25 ? "..." : null}
                    </Text>
                  </Box>
                  <Box display="flex" fontSize="xs">
                    <Text
                      fontWeight="bold"
                      color="black"
                      textColor="black"
                      mr="5px">
                      {formatIDRCurrency(item.price)}
                    </Text>
                  </Box>
                </Box>
                <Box pb="12px" px="10px" h="40px">
                  <Button
                    onClick={() => onAddCart(item.id)}
                    _hover={{
                      cursor: "pointer",
                      bg: lightGreen,
                      color: "white",
                    }}
                    w="full"
                    borderRadius="9px"
                    size="sm"
                    my="5px">
                    <Icon boxSize="4" as={IoCartOutline} mr="5px" />
                    Keranjang
                  </Button>
                </Box>
              </Box>
            );
          })}
        </Flex>
      </Center>
      <Flex justifyContent="center" alignItems="center" mt="20px" mb="20px">
        <Button
          width="100px"
          mr="2"
          _hover={{ cursor: "pointer", bg: lightGreen, color: "white" }}
          onClick={() =>
            handlePageChange(currentPage > 1 ? currentPage - 1 : 1)
          }>
          Sebelumnya
        </Button>
        {data && (
          <Text>
            Halaman {currentPage} dari {Math.ceil(data.length / itemsPerPage)}
          </Text>
        )}
        <Button
          width="100px"
          ml="2"
          _hover={{ cursor: "pointer", bg: lightGreen, color: "white" }}
          onClick={() =>
            handlePageChange(
              currentPage < Math.ceil(data.length / itemsPerPage)
                ? currentPage + 1
                : currentPage
            )
          }>
          Selanjutnya
        </Button>
      </Flex>
    </>
  );
}
