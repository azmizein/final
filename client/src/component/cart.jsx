import {
  Flex,
  Box,
  Text,
  Button,
  Icon,
  Tooltip,
  Image,
  Divider,
  useColorModeValue,
  FormControl,
  FormLabel,
  Select,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  Input,
  ModalFooter,
} from "@chakra-ui/react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import Axios from "axios";
import { FaTrashAlt } from "react-icons/fa";
import { delCart } from "../redux/userSlice";
import { cartSync } from "../redux/cartSlice";
import { useEffect, useRef, useState } from "react";
// import { v4 as uuidv4 } from "uuid";
import Swal from "sweetalert2";
import { setStatus } from "../redux/statusSlice";

export default function CartDetail() {
  const dispatch = useDispatch();
  const url = "http://localhost:2000/transaction";
  const [snap, setSnap] = useState(null);
  const [courier, setCourier] = useState("");
  const [shippingCost, setShippingCost] = useState(null);
  const [addressData, setAddressData] = useState("");
  const [selectedShippingCost, setSelectedShippingCost] = useState("");
  const [isEditing, setIsEditing] = useState(false);
  const [province, setProvince] = useState([]);
  const [city, setCity] = useState([]);
  const [selectCity, setSelectCity] = useState(0);
  const [selectProvince, setSelectProvince] = useState(0);
  const inputAddressFill = useRef("");
  const inputDistrict = useRef("");
  const inputPostal = useRef("");
  const data = useSelector((state) => state.cartSlice.value);
  const { id, username, email, cart, phoneNumber } = useSelector(
    (state) => state.userSlice.value
  );

  /// Midtrans

  useEffect(() => {
    const snapScript = document.createElement("script");

    snapScript.src = "https://app.sandbox.midtrans.com/snap/snap.js";
    snapScript.async = true;
    snapScript.onload = () => {
      if ("snap" in window) {
        const { snap } = window;
        setSnap(snap);
      }
    };
    snapScript.dataset.clientKey = "SB-Mid-client-5giWJrCY5bo_j3S0";
    document.head.appendChild(snapScript);

    return () => {
      document.head.removeChild(snapScript);
    };
  }, []);

  /// Format Indonesia Rupiah

  const formatIDRCurrency = (amount) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(amount);
  };

  /// Process Button
  const process = async () => {
    try {
      let date = new Date();
      date.setDate(date.getDate() + 5);
      let tahun = date.getFullYear();
      const numberInv = `MC-${tahun}${data.length + 1}`;
      const total = getTotalCartCost();
      const dataPay = {
        name: username,
        id: numberInv,
        email: email,
        total,
      };

      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };

      const response = await Axios.post(
        "http://localhost:2000/payment/paymentReq",
        dataPay,
        config
      );

      if (snap) {
        const { token } = response.data;

        snap.pay(token, {
          onSuccess: async function (result) {
            console.log("Payment success:", result);
            const status = result.transaction_status;
            const noInvoice = result.order_id;

            const res = await Axios.post(url, {
              total: getTotalCartCost(),
              id,
              data,
              noInvoice,
              status,
            });
            dispatch(setStatus(res.data));
            console.log(res);
            window.location.href = "/transaksi";
          },
          onPending: async function (result) {
            console.log("Payment pending:", result);
            const status = result.transaction_status;
            const noInvoice = result.order_id;

            const res = await Axios.post(url, {
              total: getTotalCartCost(),
              id,
              data,
              noInvoice,
              status,
            });
            dispatch(setStatus(res.data));
            console.log(res.data);
            window.location.href = "/transaksi";
          },
          onError: function (result) {
            console.error("Payment error:", result);
          },
        });
      }
    } catch (error) {
      console.log(error);
    }
  };

  /// Delete Cart
  const onDeleteCart = async (id) => {
    try {
      await Axios.delete(`http://localhost:2000/cart/${id}`);
      const result = await Axios.get(`http://localhost:2000/cart/${id}`);
      dispatch(delCart());
      dispatch(cartSync(result.data));
      window.location.pathname = "/cart";
    } catch (err) {
      console.log(err);
    }
  };

  /// Quantity
  const [productQuantities, setProductQuantities] = useState(
    data.reduce((quantities, item) => {
      quantities[item.id] = 1;
      return quantities;
    }, {})
  );

  const calculateTotalCost = (quantity, price) => {
    return quantity * price;
  };

  const getTotalCartCost = () => {
    let totalCost = 0;
    data.forEach((item) => {
      const quantity = productQuantities[item.id] || 1;
      totalCost += calculateTotalCost(quantity, item.Product.price);
    });

    if (selectedShippingCost) {
      totalCost += Number(selectedShippingCost);
    }
    return totalCost;
  };

  const onIncreaseQuantity = (id) => {
    setProductQuantities((prevQuantities) => ({
      ...prevQuantities,
      [id]: (prevQuantities[id] || 0) + 1,
    }));
  };

  const onDecreaseQuantity = (id) => {
    setProductQuantities((prevQuantities) => ({
      ...prevQuantities,
      [id]: Math.max((prevQuantities[id] || 0) - 1, 1),
    }));
  };

  /// Shipping

  const handleCalculateShipping = async () => {
    try {
      const response = await Axios.post(
        "http://localhost:2000/payment/shipping",
        {
          origin,
          destination: addressData?.cityId,
          courier,
        }
      );

      setShippingCost(response.data.rajaongkir.results[0].costs);
    } catch (err) {
      console.log(err);
      Swal.fire({
        icon: "error",
        title: "Oops...",
        text: `${err.response.data.error}`,
        timer: 1000,
        customClass: {
          container: "my-swal",
        },
      });
    }
  };

  /// Address

  const getDataAddress = async () => {
    try {
      const result = await Axios.get(
        `http://localhost:2000/user/getAddress/${id}`
      );
      setAddressData(result.data[0]);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getDataAddress();
  }, [id]);

  /// Shipping

  const handleSelectChange = (event) => {
    setSelectedShippingCost(event.target.value);
  };

  const getProvince = async () => {
    try {
      const response = await Axios.get(
        "http://localhost:2000/payment/province"
      );
      setProvince(response.data.cities);
    } catch (err) {
      console.log(err);
    }
  };

  const rendProvince = () => {
    return province.map((valProp) => {
      return (
        <option
          value={valProp.province_id}
          key={valProp.province_id.toString()}>
          {valProp.province}
        </option>
      );
    });
  };

  const getCity = async () => {
    try {
      const response = await Axios.get(
        `http://localhost:2000/payment/city/${selectProvince}`
      );
      setCity(response.data);
    } catch (err) {
      console.log(err);
    }
  };

  const rendCity = () => {
    return Array.from(city).map((valueProp, i) => {
      return (
        <option value={valueProp.city_id} key={i}>
          {valueProp.type + " "} {valueProp.city_name}
        </option>
      );
    });
  };

  const provinceHandle = ({ target }) => {
    const { value } = target;
    setSelectProvince(value);
  };

  const cityHandle = ({ target }) => {
    const { value } = target;
    setSelectCity(value);
  };

  useEffect(() => {
    getProvince();
  }, []);

  useEffect(() => {
    getCity();
  }, [selectProvince]);

  /// Create Address

  const openEditModal = () => {
    setIsEditing(true);
  };

  const closeEditModal = () => {
    setIsEditing(false);
  };

  const editProfile = async () => {
    if (!id) {
      Swal.fire({
        icon: "error",
        title: "Tidak Ada Akses",
        text: "Perlu Login Terlebih Dahulu",
      });
      closeEditModal();

      return;
    }

    try {
      const editData = {
        address: inputAddressFill.current.value,
        city: selectCity,
        district: inputDistrict.current.value,
        province: selectProvince,
        postal_code: inputPostal.current.value,
        UserId: id,
      };

      const editAdddress = await Axios.post(
        `http://localhost:2000/user/createAddress/${id}`,
        editData
      );

      Swal.fire({
        icon: "success",
        title: "Berhasil",
        text: "Sukses Masukan Alamat",
        timer: 2000,
      });

      console.log(editAdddress);
      closeEditModal();
      window.location.reload();
    } catch (err) {
      console.error(err);
    }
  };

  /// Color
  const lightGreen = useColorModeValue("teal.200", "teal.700");

  return (
    <Box>
      <Box
        display="flex"
        justifyContent="center"
        flexWrap={"wrap"}
        mb="30px"
        mt="20px">
        <Box
          bg={lightGreen}
          h="700px"
          p="25px"
          minW="370px"
          w={"22vw"}
          mx="15px"
          mt="10px"
          justifyContent={"center"}
          boxShadow="md"
          borderWidth="1px"
          borderRadius="10px">
          <Text fontWeight="bold" fontSize="lg">
            Keterangan
          </Text>
          <Box display="flex" mt="10px" justifyContent="space-between">
            <Text fontWeight="semibold">Nama :</Text>
            <Text fontWeight="semibold">{username}</Text>
          </Box>
          <Box
            display="flex"
            mt="10px"
            justifyContent="space-between"
            mb="10px">
            <Text fontWeight="semibold">Email :</Text>
            <Text fontWeight="semibold">{email}</Text>
          </Box>
          <Box
            display="flex"
            mt="10px"
            justifyContent="space-between"
            mb="10px">
            <Text fontWeight="semibold">Nomor Telepon :</Text>
            <Text fontWeight="semibold">{phoneNumber}</Text>
          </Box>

          <Box
            display="flex"
            mt="10px"
            justifyContent="space-between"
            mb="10px">
            <Text fontWeight="semibold" onClick={openEditModal}>
              Masukan Alamat :
            </Text>
            <Text
              fontWeight="semibold"
              cursor="pointer"
              color="white"
              onClick={openEditModal}
              _hover={{
                textDecoration: "underline",
                color: "red",
              }}>
              Klik di sini
            </Text>
          </Box>

          <Text color="white" mb="10px" fontWeight="semibold">
            {addressData
              ? `${addressData.addressFill}, ${addressData.district}, ${addressData.city}, ${addressData.province}.`
              : "Masukan alamat terlebih dahulu"}
          </Text>

          <Divider />
          <Box p={4} borderWidth="1px" borderRadius="md" mt="10px">
            <Text fontSize="xl" fontWeight="bold" mb={4}>
              Pilih Kurir
            </Text>
            <FormControl>
              <FormLabel>Pilih Layanan</FormLabel>
              <Select
                value={courier}
                onChange={(e) => setCourier(e.target.value)}>
                <option value="">Pilih Layanan</option>
                <option value="jne">JNE</option>
                <option value="tiki">TIKI</option>
                <option value="pos">POS</option>
              </Select>
              <Box mt={4}>
                <Box>
                  {shippingCost && (
                    <Box>
                      <h3>Biaya Pengiriman :</h3>
                      <Select
                        value={selectedShippingCost}
                        onChange={handleSelectChange}>
                        <option value="">Pilih Biaya</option>
                        {shippingCost.map((cost) => (
                          <option key={cost.service} value={cost.cost[0].value}>
                            {cost.service}:{" "}
                            {formatIDRCurrency(cost.cost[0].value)}
                            {","} {cost.cost[0].etd} {"Hari"}
                          </option>
                        ))}
                      </Select>
                    </Box>
                  )}
                </Box>
              </Box>
              <Button height="30px" mt="10px" onClick={handleCalculateShipping}>
                Cari Biaya
              </Button>
            </FormControl>
          </Box>

          <Box display="flex" mt="0px" justifyContent="space-between">
            <Text fontWeight="bold">Jumlah Item :</Text>
            <Text fontWeight="bold">{cart}</Text>
          </Box>
          <Box display="flex" mt="10px" justifyContent="space-between">
            <Text fontWeight="bold">Total Belanja :</Text>
            <Text fontWeight="bold">
              {formatIDRCurrency(getTotalCartCost())}
            </Text>
          </Box>
          <Box mt="10px" display="flex" justifyContent="flex-end">
            <Button
              onClick={process}
              w="full"
              borderColor="teal"
              borderRadius="9px"
              borderWidth="2px"
              _hover={{ bg: lightGreen }}
              isDisabled={data.length === 0}>
              Beli
            </Button>
          </Box>
        </Box>

        <Box
          minW="370px"
          h="auto"
          w={"55vw"}
          mt="10px"
          p="25px"
          justifyContent={"center"}
          boxShadow="md"
          borderWidth="1px"
          borderRadius="10px">
          {data.length === 0 ? (
            <>
              <Box align="center">
                <Image
                  src="https://media.giphy.com/media/2C6v4QD5d3YOO4YhID/giphy.gif"
                  objectFit="contain"
                  w="400px"
                  h="300px"
                />
                <Text textAlign="center" fontWeight="bold">
                  Keranjang anda kosong
                </Text>
                <Text
                  as={Link}
                  to="/"
                  textAlign="center"
                  fontWeight="bold"
                  color="teal.400"
                  w="150px"
                  _hover={{ cursor: "pointer", textDecoration: "underline" }}>
                  Beli Sekarang
                </Text>
              </Box>
            </>
          ) : (
            <Box
              boxShadow="sm"
              borderWidth="1px"
              borderRadius="10px"
              mb="20px"
              p="10px"
              _hover={{ boxShadow: "lg" }}>
              {data.map((item) => {
                const quantity = productQuantities[item.id] || 1;
                const totalCost = calculateTotalCost(
                  quantity,
                  item.Product.price
                );
                return (
                  <>
                    <Flex justifyContent="space-between">
                      <Box display="flex">
                        <Box
                          minW="100px"
                          minH="100px"
                          overflow="hidden"
                          borderWidth="1px">
                          <Box
                            h="50px"
                            as={Link}
                            to={`/detail/${item.Product.id}`}>
                            <Image
                              objectFit="cover"
                              src={`http://localhost:2000/public/${item.Product.images}`}
                              width="100px"
                              height="100px"
                            />
                          </Box>
                        </Box>
                        <Box
                          ml={{ base: "15px", md: "30px" }}
                          w={{ base: "100%", md: "200px" }}
                          alignItems={{ base: "flex-start", md: "center" }}
                          display="flex">
                          <Box
                            h="50px"
                            as={Link}
                            to={`/detail/${item.Product.id}`}>
                            <Text fontWeight="semibold">
                              {item.Product.name}
                            </Text>
                            <Text
                              fontWeight="semibold"
                              fontSize="small"
                              color="pink.400">
                              {formatIDRCurrency(totalCost)}
                            </Text>
                          </Box>
                        </Box>
                      </Box>
                      <Box display="flex" alignItems="center">
                        <Button
                          variant="link"
                          color="pink.400"
                          size="sm"
                          onClick={() => onDecreaseQuantity(item.id)}
                          _hover={{ color: "pink" }}>
                          -
                        </Button>
                        <Text fontWeight="semibold" fontSize="small">
                          {quantity}
                        </Text>
                        <Button
                          variant="link"
                          color="pink.400"
                          size="sm"
                          onClick={() => onIncreaseQuantity(item.id)}
                          _hover={{ color: "pink" }}>
                          +
                        </Button>
                      </Box>
                      <Tooltip label="Hapus Dari Keranjang" fontSize="sm">
                        <Button
                          variant="link"
                          color="pink.400"
                          size="sm"
                          onClick={() => onDeleteCart(item.id)}
                          _hover={{ color: "pink" }}>
                          <Icon boxSize={4} as={FaTrashAlt} />
                        </Button>
                      </Tooltip>
                    </Flex>
                    <Divider my="20px" />
                  </>
                );
              })}
            </Box>
          )}
        </Box>
      </Box>
      <Modal isOpen={isEditing} onClose={closeEditModal} size="md">
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Masukan Alamat</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <FormControl>
              <FormLabel>Alamat</FormLabel>
              <Input ref={inputAddressFill} placeholder="Jalan" />
            </FormControl>
            <FormControl>
              <FormLabel>Kecamatan</FormLabel>
              <Input ref={inputDistrict} placeholder="Kecamatan" />
            </FormControl>
            <FormControl>
              <FormLabel>Provinsi</FormLabel>
              <Select
                defaultValue={province}
                placeholder="pilih provinsi"
                onChange={provinceHandle}>
                {rendProvince()}
              </Select>
            </FormControl>
            <FormControl>
              <FormLabel>Kota</FormLabel>
              <Select
                defaultValue={city}
                placeholder="pilih kota"
                onChange={cityHandle}>
                {rendCity()}
              </Select>
            </FormControl>
            <FormControl>
              <FormLabel>Kode Pos</FormLabel>
              <Input ref={inputPostal} />
            </FormControl>
          </ModalBody>
          <ModalFooter>
            <Button colorScheme="teal" mr={3} onClick={editProfile}>
              Save
            </Button>
            <Button variant="ghost" onClick={closeEditModal}>
              Cancel
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
}
