const cart = require("./cart");
const user = require("./user");
const admin = require("./admin");
const transaction = require("./transaction");
const product = require("./product");
const payment = require("./payment");
const post = require("./post");
const message = require("./message");
const conversation = require("./conversation");
const doctor = require("./doctor");

module.exports = {
  user,
  cart,
  admin,
  transaction,
  product,
  payment,
  post,
  message,
  conversation,
  doctor,
};
