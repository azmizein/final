const userRoutes = require("./user");
const cartRoutes = require("./cart");
const adminRoutes = require("./admin");
const transRoutes = require("./transaction");
const productRoutes = require("./product");
const paymentRoutes = require("./payment");
const postRoutes = require("./post");
const transactionRoutes = require("./transaction");
const messageRoutes = require("./message");
const conversationRoutes = require("./conversation");
const doctorRoutes = require("./doctor");

module.exports = {
  userRoutes,
  cartRoutes,
  adminRoutes,
  transRoutes,
  productRoutes,
  paymentRoutes,
  postRoutes,
  transactionRoutes,
  messageRoutes,
  conversationRoutes,
  doctorRoutes,
};
